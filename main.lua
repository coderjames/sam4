local bump       = require 'bump'
local gamera     = require 'gamera'
local anim8      = require 'anim8'
local bit        = require 'numberlua'

local levelData  = require 'SecretAgent'

--[[
Near-term Task List (unordered):
--------------------------------
- spikes that retract and only harm player when visible
- multiple levels

Later Tasks:
------------
- Using teleporters
- enemies that shoot projectiles (ceiling crawler, crazed zombie)
- player health
- overworld

Bonus Tasks:
------------
- Player bonus score for picking up S-P-Y items in proper order
- player bonus score for completing level with full health
- high score list
- moving platforms
- switch that turns lights on or off
- switch that enables moving platforms to move or be stationary
- multitile enemies (tall robots and wide dogs)
- menu
- configurable keybindings
- save and restore game
- high score list
- help screen
- intro / story screen
- sounds (shooting, walking, dying)


]]

local instructions = [[
  bump.lua simple demo

    arrows: move
    tab: toggle debug info
    delete: run garbage collector
]]

local cols_len = 0 -- how many collisions are happening
local shouldDrawDebug = false

-- this is either false or a function to invoke to draw the popup
local displayPopup = false --function () end

local Y_SPEED_DELTA = 15
local Y_SPEED_MAXIMUM = 120
local Y_SPEED_JUMP = -380
local X_SPEED_GROUND = 120
local X_SPEED_AIR    = 120

local satelliteHealth = 3

-- scaling up all the source 16x16 tiles this amount
local TILE_SCALE_FACTOR = 2

-- World creation
local inventoryIcons = {}
local world = bump.newWorld()
local camera = gamera.new(0,0,
                          levelData.levels[0].MapWidth * levelData.TileWidth * TILE_SCALE_FACTOR,
                          levelData.levels[0].MapHeight* levelData.TileHeight* TILE_SCALE_FACTOR)

-- Message/debug functions
local function drawMessage()
  local msg = instructions:format(tostring(shouldDrawDebug))
  love.graphics.setColor(255, 255, 255)
  love.graphics.print(msg, 550, 10)
end

local consoleBuffer = {}
local consoleBufferSize = 15
for i=1,consoleBufferSize do consoleBuffer[i] = "" end
local function consolePrint(msg)
  table.remove(consoleBuffer,1)
  consoleBuffer[consoleBufferSize] = msg
end

local function drawConsole()
  local str = table.concat(consoleBuffer, "\n")
  for i=1,consoleBufferSize do
    love.graphics.setColor(255,255,255, i*255/consoleBufferSize)
    love.graphics.printf(consoleBuffer[i], 10, 580-(consoleBufferSize - i)*12, 790, "left")
  end
end

local function drawPopupWindow(width, height, border, message)
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.rectangle( "fill",
                           (love.graphics.getWidth( ) - width) / 2,
                           (love.graphics.getHeight( ) - height) / 2,
                           width,
                           height)
    
  width = width - (border * 2)
  height = height - (border * 2)
  love.graphics.setColor(2, 83, 222, 255)
  love.graphics.rectangle( "fill",
                           (love.graphics.getWidth( ) - width) / 2,
                           (love.graphics.getHeight( ) - height) / 2,
                           width,
                           height)

  local line_y = (love.graphics.getHeight( ) - height) / 2
  local line_x = (love.graphics.getWidth( ) - width) / 2 + 20
  love.graphics.setColor(255, 255, 255, 255)   
  love.graphics.setFont(guiFont)
  for i,s in ipairs(message) do
    love.graphics.print(s, line_x, line_y + (i * 20))
  end
end

local function drawNeedDynamitePopup()
  drawPopupWindow(400, 500, 20,
                  {'You need to pick up the dynamite',
                   'in order to destroy this door', })
end

local function drawDestroySatellitePopup()
  drawPopupWindow(400, 500, 20,
                  {'Must destroy the satellite dish',
                   'to complete your mission before',
                   'exiting the level, Samantha.'})
end

-- Player functions
player =
{
    x=50,
    y=50,
    w=levelData.TileWidth * TILE_SCALE_FACTOR - 1,
    h=levelData.TileHeight * TILE_SCALE_FACTOR,
    x_speed = X_SPEED_GROUND,
    y_speed = Y_SPEED_MAXIMUM,
    on_ground = false,
    animationName = "standing",
    facingDirection = "right",
    hasGlasses = false,
    health = 3,
    ammo = 0,
    score = 0,
    hasKey = { red=false, green=false, blue=false},
    hasDisk = false,
    hasDynamite = false,
    laserOn = true,
    dynamiteTimer = nil,
    hasSeenNeedDynamitePopup = false,
    hasSeenDestroySatellitePopup = false
}

SHOT_SPEED = 120
shot =
{
    x = 0,
    y = 0,
    w = 8,
    h = 4,
    speed = SHOT_SPEED,
    damage = 1,
    alive = false,
    isShot = true
}

function playerDoorCollision(mapCode, keyName, door)
    if door.MapCode == mapCode then -- closed door
        if player.hasKey[keyName] == true then
            return 'cross'
        else
            return 'slide'
        end
    end    
end

local playerItemFilter = function(playerItem, other)
    -- ignore invisible platforms unless the player has the glasses
    if other.MapCode == 4 and (player.hasGlasses == false) then return false end
    if other.isShot == true then return false end
    
    local boundsFlags = other.bounds
    local solidTop    = bit.btest(boundsFlags, 0x01)
    local solidLeft   = bit.btest(boundsFlags, 0x02)
    local solidBottom = bit.btest(boundsFlags, 0x04)
    local solidRight  = bit.btest(boundsFlags, 0x08)
    local px = playerItem.x
    local py = playerItem.y
     
    if (py+playerItem.h-1.0)< other.y and      --[[player above other]]
                     solidTop == true and      --[[other solid on top]]
                  playerItem.y_speed >= 0 then --[[player moving downward]]
       return 'slide'                          --[[don't let player through]]
    end
    if other.y < py and solidBottom == true then return 'slide' end
    
    if px < other.x and solidLeft == true then return 'slide' end
    if other.x < px and solidRight == true then return 'slide' end
    
    local doorCollision = playerDoorCollision(11, "red", other)
    if doorCollision then return doorCollision end
    
    doorCollision = playerDoorCollision(12, "green", other)
    if doorCollision then return doorCollision end
    
    doorCollision = playerDoorCollision(13, "blue", other)
    if doorCollision then return doorCollision end
    
    if other.MapCode ~= nil and other.MapCode ~= 0 then
        return 'cross'
    end
  
    -- else return nil (automatically) meaning objects do not collide
end

local function playerDied()
    player.x = 40
    player.y = 40
    world:update(player, player.x, player.y, player.w, player.h)
end

local function playerCompletedLevel()
    player.score = player.score + 1000
    loadLevel(1)
    world:update(player, player.x, player.y, player.w, player.h)
end

local function updatePlayer(dt)
  if player.on_ground then
      player.x_speed = X_SPEED_GROUND
  else
      player.x_speed = X_SPEED_AIR
  end
  
  local speed = player.x_speed

  local dx, dy = 0, 0
  if love.keyboard.isDown('right') then
    dx = speed * dt
    
    player.facingDirection = "right"
  elseif love.keyboard.isDown('left') then
    dx = -speed * dt
    
    player.facingDirection = "left"
  end
  
  if love.keyboard.isDown('space') and player.on_ground then
    player.y_speed = Y_SPEED_JUMP
  else
    player.y_speed = player.y_speed + Y_SPEED_DELTA;
  end
  
  if love.keyboard.isDown('x') and shot.alive == false and player.ammo > 0 then
    if player.facingDirection == "right" then
        shot.x = player.x + player.w
        shot.speed = SHOT_SPEED
    else
        shot.x = player.x - shot.w - 2
        shot.speed = -SHOT_SPEED
    end
    
    shot.y = player.y + 16
    shot.alive = true
    player.ammo = player.ammo - 1
    world:add(shot, shot.x, shot.y, shot.w, shot.h)
  end
  
  player.y_speed = math.min(player.y_speed, Y_SPEED_MAXIMUM);
  
  dy = player.y_speed * dt;
  
  player.on_ground = false
  if dx ~= 0 or dy ~= 0 then
    local cols
    player.x, player.y, cols, cols_len = world:move(player, player.x + dx, player.y + dy, playerItemFilter)
    for i=1, cols_len do
      local col = cols[i]
      local other = col.other

      if col.type == 'slide' and col.normal.y < 0 then
        player.on_ground = true
        player.y_speed = 0
      end
      if col.type == 'slide' and col.normal.y > 0 then player.y_speed = 0 end
    
      if col.type == 'cross' then    
        if other.MapCode == 1 then
          -- Stepped on explosive mine. Die.
          playerDied()
        elseif other.MapCode == 2 then
          -- Stepped on ammo. Add to player ammo. Remove item from bump world and items table
            player.ammo = player.ammo + 5
            player.score = player.score + 200
            removeBlock(other)
        elseif other.MapCode == 3 then
            -- picked up the glasses.
            player.hasGlasses = true
            removeBlock(other)
        -- RESERVED: MapCode == 4 is invisible platforms made visible by the glasses
        elseif other.MapCode == 5 then -- moneybag
            player.score = player.score + 500
            removeBlock(other)
        elseif other.MapCode == 6 then -- satellite dish
            -- nothing
        elseif other.MapCode == 7 then --  white-suit
            player.health = player.health - 1
            -- other.x_speed = -other.x_speed
        elseif other.MapCode == 8 then -- red key
            player.hasKey["red"] = true
            removeBlock(other)
        elseif other.MapCode == 9 then -- green key
            player.hasKey["green"] = true
            removeBlock(other)
        elseif other.MapCode == 10 then -- blue key
            player.hasKey["blue"] = true
            removeBlock(other)
        elseif other.MapCode == 11 then -- red door and player has key
            player.hasKey["red"] = false
            removeBlock(other)
        elseif other.MapCode == 12 then -- green door and player has key
            player.hasKey["green"] = false
            removeBlock(other)
        elseif other.MapCode == 13 then -- blue door and player has key
            player.hasKey["blue"] = false
            removeBlock(other)
        elseif other.MapCode == 14 then -- disk
            player.hasDisk = true
            removeBlock(other)
        elseif other.MapCode == 15 then -- dynamite
            player.hasDynamite = true
            player.hasSeenNeedDynamitePopup = true
            removeBlock(other)
        elseif other.MapCode == 16 then -- laser generator endpoint
            if player.laserOn == true then
                playerDied()
            end
        elseif other.MapCode == 17 then -- laser
            playerDied()
        elseif other.MapCode == 18 then -- computer
            if player.hasDisk == true then
                player.laserOn = false

                for _,b in ipairs(blocks) do
                    if b.MapCode == 17 then
                        removeBlock(b)
                    end
                end
            end
        elseif other.MapCode == 19 or -- top half of exploded exit door
               other.MapCode == 20 then   -- bottom half of exploded exit door
          if satelliteHealth <= 0 then -- allowed to leave the level
            playerCompletedLevel()
          elseif player.hasSeenDestroySatellitePopup == false then
            displayPopup = drawDestroySatellitePopup
            player.hasSeenDestroySatellitePopup = true
          end
        -- RESERVED: MapCode == 21 is exploding dynamite
        end -- if-elseif tree of mapcodes
      end -- if col.type == 'cross'

      -- check for collision with exit door outside of collision type being 'cross'
      -- so that the exit door can be marked solid. This way the player bounces off,
      -- the dynamite gets set where the player is, and the dynamite ends up at the
      -- base of the door regardless of which side the player touchs it from.
      -- So, this check of the exit door is when it is marked solid, so it is not yet
      -- exploded by the dynamite.
      if col.type == 'slide' and other.MapCode == 20 then -- bottom half of exit door
        if player.hasDynamite == true then
            -- create the dynamite block and add it to the world
            -- keeping a reference to it outside the blocks table for easier
            -- finding once the timer expires
            player.dynamite = addBlock(player.x,player.y,
                     levelData.TileWidth  * TILE_SCALE_FACTOR,
                     levelData.TileHeight  * TILE_SCALE_FACTOR,
                     0,21,
                     {-1,268,-1})
            player.hasDynamite = false
            player.dynamiteTimer = 0.0
        else
          if player.hasSeenNeedDynamitePopup == false then
            displayPopup = drawNeedDynamitePopup
            player.hasSeenNeedDynamitePopup = true
          end
        end
      end

      consolePrint(("col.other = %s, col.type = %s, col.normal = %d,%d"):format(col.other, col.type, col.normal.x, col.normal.y))
    end -- for (each collision)
  end -- if (dx ~= 0 or dy ~= 0)
  
  if player.dynamiteTimer ~= nil then
    player.dynamiteTimer = player.dynamiteTimer + dt
    if player.dynamiteTimer > 3.0 then
        -- remove dynamite block from world
        removeBlock(player.dynamite)
        player.dynamite = nil
        
        for _,b in ipairs(blocks) do
          if b.layerIcons[2] == 265 then -- unexploded exit door top
            b.layerIcons[2] = 266 -- exploded exit door top
            b.bounds = 0
          elseif b.layerIcons[2] == 293 then -- unexploded exit door bottom
            b.layerIcons[2] = 294 -- exploded exit door bottom
            b.bounds = 0
          end
        end
        
        player.dynamiteTimer = nil
    end
  end
  
  if player.on_ground == true then
    if dx ~= 0 then
        player.animationName = "walking"
    else
        player.animationName = "standing"
    end
  else
    player.animationName = "jumping"
  end
  camera:setPosition(player.x, player.y)
  player.animation[string.format('%s_%s', player.animationName, player.facingDirection)]:update(dt)
end

local shotItemFilter = function(shotItem, other)
    if other == player then return false end
    
    local boundsFlags = other.bounds
    local solidLeft   = bit.btest(boundsFlags, 0x02)
    local solidRight  = bit.btest(boundsFlags, 0x08)
    
    if shotItem.x < other.x and solidLeft == true then return 'touch' end
    if other.x < shotItem.x and solidRight == true then return 'touch' end
    
    if other.MapCode ~= nil and other.MapCode ~= 0 then
        return 'cross'
    end
  
    -- else return nil (automatically) meaning objects do not collide
end

local function updateShot(dt)
    if shot.alive == true then
        local dx = shot.speed * dt;
        local cols
        shot.x, shot.y, cols, cols_len = world:move(shot, shot.x + dx, shot.y, shotItemFilter)
    
        for i=1, cols_len do
            local col = cols[i]
            local other = col.other

            if col.type == 'touch' then
                -- hit something solid. shot is dead
                shot.alive = false
            elseif col.type == 'cross' then
                -- hit something interesting, like an enemy. investigate further.
                shot.alive = false
                
                if other.MapCode == 5 then -- moneybag
                    player.score = player.score + 500
                    removeBlock(other)
                elseif other.MapCode == 6 then -- satellite dish
                    satelliteHealth = satelliteHealth - 1
                    if satelliteHealth <= 0 then
                        removeBlock(other)
                        player.score = player.score + 1000
                    else
                        player.score = player.score + 500
                    end
                elseif other.MapCode == 7 then --  white-suit
                    -- turn in to tombstone
                    other.MapCode = 5
                    other.layerIcons[2] = (38*16+3)
                    -- move from mobiles table to blocks table
                    removeMobile(other)
                    blocks[#blocks+1] = other
                end
            end
        end    
        
        -- remove the shot from the bump world once at the end of dealing with
        -- all collisions, in case more than one collision will cause it to be
        -- removed.
        if shot.alive == false then
            world:remove(shot)
        end
    end
end

local function drawPlayer()
  player.animation[string.format('%s_%s', player.animationName, player.facingDirection)]:draw(tileAtlas, player.x, player.y)
    
  if shouldDrawDebug then    
    love.graphics.setColor(0, 255, 0, 240)
    love.graphics.rectangle( "line", player.x, player.y, player.w, player.h )
  end
  
  love.graphics.setColor(255, 255, 255, 255)
end

local function drawDebug()
  local statistics = ("fps: %d, mem: %dKB, collisions: %d, items: %d"):format(love.timer.getFPS(), collectgarbage("count"), cols_len, world:countItems())
  love.graphics.setColor(255, 255, 255)
  love.graphics.printf(statistics, 0, 580, 790, 'right')
end

-- Block functions

blocks = {}
mobiles = {}

local blockUpdate = function (self, dt)
    self.icon:update(dt)
    
    local dx, dy = self.x_speed * dt, self.y_speed * dt
    if dx ~= 0 or dy ~= 0 then
        local cols
        self.x, self.y, cols = world:move(self, self.x + dx, self.y + dy, walkerItemFilter)
        for i=1, #cols do
            local col = cols[i]
            
            if col.type == 'slide' and col.normal.x ~= 0 then
                self.x_speed = -self.x_speed
            end
        end
    end
end

function addBlock(x,y,w,h,bounds,mapcode,layerIcons)
  local block = {x=x,y=y,w=w,h=h,bounds=bounds,MapCode=mapcode,layerIcons=layerIcons}
    
  -- replace tiles representing animations with actual animations
  for i=1,3 do
    if block.layerIcons[i] == 179 then -- first tile of landmine animation chain
        block.layerIcons[i] = anim8.newAnimation({tiles[179], tiles[317]}, 0.1)
    elseif block.layerIcons[i] == 386 then -- first tile of security camera chain
        block.layerIcons[i] = anim8.newAnimation({tiles[387], tiles[388], tiles[389], tiles[388]}, 1.0)
    elseif block.layerIcons[i] == 370 then -- white suite
        block.layerIcons[i] = anim8.newAnimation({tiles[370], tiles[371], tiles[372], tiles[373]}, 0.2)    
    elseif block.layerIcons[i] == 268 then -- exploding dynamite
        block.layerIcons[i] = anim8.newAnimation({tiles[268], tiles[267]}, 0.1)
    end
  end

  if block.MapCode == 7 then -- white suit
      block.x_speed = -40
      block.y_speed = 40
      block.update = blockUpdate
      block.icon = block.layerIcons[2]
      mobiles[#mobiles+1] = block
  else    
      blocks[#blocks+1] = block
  end
  
  world:add(block, x,y,w,h)
  
  return block
end

function removeBlock(block)
    for i=1,#blocks do
        if blocks[i] == block then
            table.remove(blocks, i)
            world:remove(block)
            break                                
        end
    end
end

function removeMobile(mob)
    for i=1,#mobiles do
        if mobiles[i] == mob then
            table.remove(mobiles, i)
            break                                
        end
    end
end

walkerItemFilter = function(walkerItem, other)
    -- Mobiles ignore the bullet. Deal with the collision from the
    -- bullet's perspective.
    if other.isShot == true then return false end
        
    -- Only deal with the collision from the players perspective
    if other == player then return false end
    
    local boundsFlags = other.bounds
    local solidTop    = bit.btest(boundsFlags, 0x01)
    local solidLeft   = bit.btest(boundsFlags, 0x02)
    local solidRight  = bit.btest(boundsFlags, 0x08)
    local px = walkerItem.x
    
    if solidTop == true and walkerItem.y < other.y then return 'slide' end    
    if solidLeft == true and px < other.x then return 'slide' end
    if solidRight == true and other.x < px then return 'slide' end
    
    return nil
end

-- Main LÖVE functions
tiles = {}

function loadLevel(levelNumber)
  world = bump.newWorld()
  camera = gamera.new(0,0,
                      levelData.levels[levelNumber].MapWidth * levelData.TileWidth * TILE_SCALE_FACTOR,
                      levelData.levels[levelNumber].MapHeight* levelData.TileHeight* TILE_SCALE_FACTOR)
  blocks = {}
  mobiles = {}

  satelliteHealth = 3

  player.x_speed = X_SPEED_GROUND
  player.y_speed = Y_SPEED_MAXIMUM
  player.on_ground = false
  player.animationName = "standing"
  player.facingDirection = "right"
  player.hasGlasses = false
  player.health = 3
  player.hasKey = { red=false, green=false, blue=false}
  player.hasDisk = false
  player.hasDynamite = false
  player.laserOn = true
  player.dynamiteTimer = nil
  player.hasSeenNeedDynamitePopup = false
  player.hasSeenDestroySatellitePopup = false

  local bounds = 0
  local mapcode = 0
  local layerIcons = {}
  local map = levelData.levels[levelNumber]
  local boundsInteresting = false
  local mapcodeInteresting = false
  local iconsInteresting = false
  for y = 0,map.MapHeight-1 do
    for x =0,map.MapWidth-1 do
      bounds = map.MapData[y][x][4]
      mapcode = map.MapData[y][x][5]
      layerIcons = {map.MapData[y][x][1], map.MapData[y][x][2], map.MapData[y][x][3]}
      boundsInteresting = (type(bounds) == 'number') and (bounds ~= 0)
      mapcodeInteresting = (type(mapcode) == 'number') and (mapcode ~= 0)
      iconsInteresting = (layerIcons[1] ~= -1) or (layerIcons[2] ~= -1) or (layerIcons[3] ~= -1)
      
      --[[
      Nota Bene:
        TileStudio bounds definitions are as follows:
        How to interpret the Bounds byte:
            Bit 7 = 0: 
            Bit 0: Upper bound 
            Bit 1: Left bound 
            Bit 2: Lower bound 
            Bit 3: Right bound
            
            Bit 7 = 1: 
            Bit 0: Diagonal direction: 0=/, 1=\ 
            Bit 6..1:
            0: y = x 
            Not (yet) implemented: 
            1: y = 2 * x 
            2: y = 2 * (x + 1/2) 
            3: y = x / 2 
            4: y = x / 2 + 1/2

        Taken from: http://tilestudio.sourceforge.net/tutor.html#CreateTSD
        on 2016-08-03
      ]]
      if (mapcode ~= 0xFE) and (boundsInteresting or iconsInteresting or mapcodeInteresting) then
        addBlock(x * levelData.TileWidth  * TILE_SCALE_FACTOR,
                 y * levelData.TileHeight * TILE_SCALE_FACTOR,
                 levelData.TileWidth  * TILE_SCALE_FACTOR,
                 levelData.TileHeight * TILE_SCALE_FACTOR,
                 bounds, mapcode, layerIcons)
      end
      
      -- try to find the player start location based on MapCode FE
      if mapcode == 0xFE then
        player.x = x * levelData.TileWidth  * TILE_SCALE_FACTOR
        player.y = y * levelData.TileHeight * TILE_SCALE_FACTOR
        
        -- and need to turn off the player sprite on the Action layer
        -- which is used as a reference in TileStudio to more easily
        -- identify the player start (and remind me that FE == start)
        map.MapData[y][x][2] = -1
      end
    end
  end

  world:add(player, player.x, player.y, player.w, player.h)
end

function love.load()
  love.graphics.setDefaultFilter( 'nearest', 'nearest' )
  guiFont = love.graphics.newFont("PxPlus_IBM_VGA9.ttf", 16);
  tileAtlas = love.graphics.newImage( levelData.TileSetGraphics )
  
  -- build a series of quads to represent the different tiles of the atlas
  -- tiles[number] is the quad matching that tile number so that drawing a tile
  -- can be done like:
  --
  -- love.graphics.draw(tileAtlas, tiles[tileNum], destX, destY)
  --
  -- Each quad is a source tile scaled by TILE_SCALE_FACTOR relative to
  -- levelData.TileWidth and levelData.TileHeight
  --
  
  local imgWidth, imgHeight = tileAtlas:getDimensions()
  for y = 0,levelData.VerticalTileCount do
      for x = 0,levelData.HorizontalTileCount do
          tiles[y*levelData.HorizontalTileCount+x] =
            love.graphics.newQuad(x * levelData.TileWidth  * TILE_SCALE_FACTOR,
                                  y * levelData.TileHeight * TILE_SCALE_FACTOR,
                                  levelData.TileWidth  * TILE_SCALE_FACTOR,
                                  levelData.TileHeight * TILE_SCALE_FACTOR,
                                  imgWidth * TILE_SCALE_FACTOR,
                                  imgHeight * TILE_SCALE_FACTOR)
      end
  end

  local shotTileNum = 17*16+8
  tiles[shotTileNum] = love.graphics.newQuad((8 * levelData.TileWidth + 4) * TILE_SCALE_FACTOR,
                                             (17 * levelData.TileHeight +6)* TILE_SCALE_FACTOR,
                                             shot.w  * TILE_SCALE_FACTOR,
                                             shot.h * TILE_SCALE_FACTOR,
                                             imgWidth * TILE_SCALE_FACTOR,
                                             imgHeight * TILE_SCALE_FACTOR)

  -- {x,y} tile indices into the tileAtlas that make up the player sprites.
  local spriteAdjustments =
  {
    {14, 22},
    {15, 22},
    {0, 23},
    {1, 23},
    {1, 25},
    {2, 25},
    {3, 25},
    {4, 25},
    {15,28},
    {0, 29},
    {1,27},
    {2,27}
  }
  
  local tileNum = 0
  for t = 1,#spriteAdjustments do
      tileNum = spriteAdjustments[t][2] * levelData.HorizontalTileCount + spriteAdjustments[t][1]
      
      -- Shrink the bounding quad for each of the player sprites by 5 pixels on
      -- the left and right sides to better match the used pixels
      tiles[tileNum] =
      love.graphics.newQuad(spriteAdjustments[t][1]*levelData.TileWidth  * TILE_SCALE_FACTOR+5,
                            spriteAdjustments[t][2]*levelData.TileHeight * TILE_SCALE_FACTOR,
                            levelData.TileWidth  * TILE_SCALE_FACTOR-10,
                            levelData.TileHeight * TILE_SCALE_FACTOR,
                            imgWidth * TILE_SCALE_FACTOR,
                            imgHeight * TILE_SCALE_FACTOR)
  end
  player.w = levelData.TileWidth  * TILE_SCALE_FACTOR-10
  player.h = levelData.TileHeight * TILE_SCALE_FACTOR
    
  player.animation = {}
  player.animation["walking_right"] = anim8.newAnimation({tiles[366], tiles[367], tiles[368], tiles[369]}, 0.2)
  player.animation["walking_left"]  = anim8.newAnimation({tiles[401], tiles[402], tiles[403], tiles[404]}, 0.2)
  
  player.animation["jumping_right"] = anim8.newAnimation({tiles[29*16-1]}, 0.2)
  player.animation["jumping_left"]  = anim8.newAnimation({tiles[29*16]}, 0.2)
  
  player.animation["standing_right"] = anim8.newAnimation({tiles[27*16+1]}, 0.2)
  player.animation["standing_left"]  = anim8.newAnimation({tiles[27*16+2]}, 0.2)
  
  loadLevel(0)

  math.randomseed( os.time() )
end

function love.update(dt)
  if displayPopup == false then
    cols_len = 0
    updatePlayer(dt)
    updateShot(dt)
    
    for i=1,#mobiles do
      mobiles[i]:update(dt)
    end

    local isStaticTile = false
    for i=1,#blocks do
      for layer=1,3 do
        isStaticTile = type(blocks[i].layerIcons[layer]) == 'number'

        if not(isStaticTile) then
            blocks[i].layerIcons[layer]:update(dt)
        end
      end      
    end
  end
end

local function drawLayer(layer,visible_left,visible_top,visible_right,visible_bottom)
    local insideCamera = false
    local invisiblePlatform = false
    local tileVisible = false
    local isStaticTile = false
    
    for i = 1,#blocks do
        insideCamera = (blocks[i].x >= visible_left) and (blocks[i].x <= visible_right) and
                          (blocks[i].y >= visible_top) and (blocks[i].y <= visible_bottom)
        invisiblePlatform = blocks[i].MapCode == 4

        tileVisible = insideCamera
        if layer == 2 then
            tileVisible = tileVisible and
                          (player.hasGlasses or (invisiblePlatform == false))
        end
        

        isStaticTile = type(blocks[i].layerIcons[layer]) == 'number'
        
        if blocks[i].layerIcons[layer] ~= -1 and tileVisible then
            if isStaticTile then
                love.graphics.draw(tileAtlas, tiles[blocks[i].layerIcons[layer]],
                                   blocks[i].x,
                                   blocks[i].y)
            else
                blocks[i].layerIcons[layer]:draw(tileAtlas, blocks[i].x, blocks[i].y)
            end
            if shouldDrawDebug then
                love.graphics.setColor(255, 00, 0, 160)
                love.graphics.rectangle( "fill", blocks[i].x, blocks[i].y, blocks[i].w, blocks[i].h )
                love.graphics.setColor(255, 255, 255, 255)
            end
        end
    end    
end

local function drawGUI()
    local y = love.graphics.getHeight( ) - levelData.TileHeight + 1
    
    love.graphics.setColor(0, 0, 255, 255)
    love.graphics.rectangle( "fill",
                             0, love.graphics.getHeight( ) - levelData.TileHeight,
                             love.graphics.getWidth( ), levelData.TileHeight )
    
    love.graphics.setColor(255, 255, 255, 255)   
    love.graphics.setFont(guiFont)
    love.graphics.print(string.format('Score: %06d', player.score), 32, y)
    love.graphics.print(string.format('Ammo: %d', player.ammo),   200, y)
    
    -- inventory
    if player.hasKey["red"] == true then
        love.graphics.draw(tileAtlas, tiles[10*16+10], 348, y, 0, 0.5, 0.5)
    end
    if player.hasKey["green"] == true then
        love.graphics.draw(tileAtlas, tiles[10*16+8], 364, y, 0, 0.5, 0.5)
    end
    if player.hasKey["blue"] == true then
        love.graphics.draw(tileAtlas, tiles[12*16+10], 380, y, 0, 0.5, 0.5)        
    end
    if player.hasDisk == true then
        love.graphics.draw(tileAtlas, tiles[3*16+2], 412, y, 0, 0.5, 0.5)                
    end
    if player.hasDynamite == true then
        love.graphics.draw(tileAtlas, tiles[16*16+11], 432, y, 0, 0.5, 0.5)                        
    end
    love.graphics.print(string.format('Health: %d', player.health), 600, y)
    
end

function love.draw()
    local visible_right = 0
    local visible_bottom = 0
  
    camera:draw(function(visible_left,visible_top,visible_width,visible_height)
      -- consider one more tile visible in each direction so that the partial
      -- tiles at the edges are not clipped out entirely
      visible_left = visible_left - levelData.TileWidth*TILE_SCALE_FACTOR
      visible_top  = visible_top  - levelData.TileHeight*TILE_SCALE_FACTOR
      
      visible_right = visible_left + visible_width + 2*levelData.TileWidth*TILE_SCALE_FACTOR
      visible_bottom = visible_top + visible_height + 2*levelData.TileHeight*TILE_SCALE_FACTOR
      
      -- First draw the background and action layers
      drawLayer(1,visible_left,visible_top,visible_right,visible_bottom)
      drawLayer(2,visible_left,visible_top,visible_right,visible_bottom)
      
      -- Then the player
      drawPlayer()

      for i=1,#mobiles do
        mobiles[i].icon:draw(tileAtlas, mobiles[i].x, mobiles[i].y)
      end

      -- And the shot if the player is currently shooting
      if shot.alive == true then
        love.graphics.draw(tileAtlas, tiles[17*16+8], shot.x, shot.y)
        if shouldDrawDebug then
            love.graphics.setColor(0, 0, 255, 160)
            love.graphics.rectangle( "fill", shot.x, shot.y, shot.w, shot.h )
            love.graphics.setColor(255, 255, 255, 255)
        end
      end
      -- The foreground layer goes on top of the player and the other layers
      drawLayer(3,visible_left,visible_top,visible_right,visible_bottom)
    end)  

  -- Draw any game UI elements here (level #, score, health, ammo, etc.)
  if displayPopup ~= false then
    displayPopup()
  end
  drawGUI()
  
  -- Finally, do the debug drawing last so it is always on top and visible
  if shouldDrawDebug then
    --drawBlocks()
    drawDebug()
    drawConsole()
  end

end

-- Non-player keypresses
function love.keypressed(k)
  if k=="escape" and displayPopup == false then love.event.quit() end
  if k=="escape" and displayPopup ~= false then displayPopup = false end
  
  if k=="return" and displayPopup ~= false then displayPopup = false end

  if k=="tab"    then shouldDrawDebug = not shouldDrawDebug end
  if k=="delete" then collectgarbage("collect") end
  if k=="r"      then playerDied() end
end
